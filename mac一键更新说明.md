#### 1 下载zip并解压

下载 **`mixly2.0-mac-arm64-update-tool`** ，解压到自定义位置。

#### 2 安装git

安装此目录下提供的 **git-2.15.0-intel-universal-mavericks.dmg** 安装包。

#### 3 打开终端

使用快捷键 **`command+空格`** 打开聚焦搜索，输入 **`terminal.app`** 并选中匹配项，**`Enter`** 后即可打开终端。

#### 4 切换路径到Mixly文件夹

在终端中输入 **`cd [path]`** ，其中 **`[path]`** 为所解压的mixly2.0更新文件夹路径。

例如：当前mixly2.0文件夹路径为/Users/xxx/mixly2.0，则对应指令为 **`cd /Users/xxx/mixly2.0`**。

#### 5 开启root权限

在终端中输入 **`sudo su`** ，**`Enter`** 后按照提示 **`输入密码`** 即可开启root权限，注意：以下操作必须在root下执行，否则可能会出错。

#### 6 运行一键更新脚本

在终端中输入 **`sh 一键更新.sh`**，Enter后根据提示选择需要安装的板卡，然后等待完成，安装完成后，关闭终端。